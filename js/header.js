/* JavaScript for header background slideshow */


window.addEventListener("load", function() {
    var headerOverlay = document.getElementById("overlay");
    var headerContainer = document.getElementById("header-bottom");

    var headerClassArray = new Array("bee", "tractor", "meadow");
    var index = 1;

    if ((headerContainer !== null) && (headerOverlay !== null)) {
        setInterval(function() {
            headerOverlay.style.opacity = "1";

            setTimeout(function() {
                headerContainer.className = headerClassArray[index];
                index = (index + 1) % headerClassArray.length;

                setTimeout(function() {
                    headerOverlay.style.opacity = "0";
                }, 500);
            }, 2000);
        }, 8000);
    }
});
