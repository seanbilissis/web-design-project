/* JavaScript for gallery.html */

function changeImage(n) {
     
    var galleryImages = document.getElementsByClassName("gallery-image");
    var modalImage = document.getElementById("modal-image");
    var index = Number(modalImage.className.substr(-1));

    index = (index + n) % galleryImages.length; 
    setModalImage(galleryImages[((index < 0) ? (index + galleryImages.length) : index)]);
}


function setModalImage(x) {

    var modalImage = document.getElementById("modal-image");
    var modalCaption = document.getElementById("modal-caption");

    modalImage.src = x.src;
    modalImage.className = x.id;
    modalCaption.innerHTML = x.alt;
}


function attachHandlers() {

    var galleryImages = document.getElementsByClassName("gallery-image");
    var span = document.getElementById("close-button");
    var next = document.getElementsByName("button-next")[0];
    var prev = document.getElementsByName("button-prev")[0];

    next.addEventListener("click", function(){changeImage(1);});
    prev.addEventListener("click", function(){changeImage(-1);});
    
    span.addEventListener("click", function() {
        modal.style.display = "none";
    });

    for (let i = 0; i < galleryImages.length; i++) {
        galleryImages.item(i).addEventListener("click", function() {
            modal.style.display = "block"; 
            setModalImage(this);
        });
    }
}

window.addEventListener("load", attachHandlers);